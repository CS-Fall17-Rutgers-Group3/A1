//
// Copyright (c) 2009-2015 Glen Berseth, Mubbasir Kapadia, Shawn Singh, Petros Faloutsos, Glenn Reinman
// See license.txt for complete license.
// Copyright (c) 2015 Mahyar Khayatkhoei
//

#include <algorithm>
#include <vector>
#include <util/Geometry.h>
#include <util/Curve.h>
#include <util/Color.h>
#include <util/DrawLib.h>
#include "Globals.h"

using namespace Util;

Curve::Curve(const CurvePoint& startPoint, int curveType) : type(curveType)
{
	controlPoints.push_back(startPoint);
}

Curve::Curve(const std::vector<CurvePoint>& inputPoints, int curveType) : type(curveType)
{
	controlPoints = inputPoints;
	sortControlPoints();
}

// Add one control point to the vector controlPoints
void Curve::addControlPoint(const CurvePoint& inputPoint)
{
	controlPoints.push_back(inputPoint);
	sortControlPoints();
}

// Add a vector of control points to the vector controlPoints
void Curve::addControlPoints(const std::vector<CurvePoint>& inputPoints)
{
	for (int i = 0; i < inputPoints.size(); i++)
		controlPoints.push_back(inputPoints[i]);
	sortControlPoints();
}

// Draw the curve shape on screen, usign window as step size (bigger window: less accurate shape)
void Curve::drawCurve(Color curveColor, float curveThickness, int window)
{
#ifdef ENABLE_GUI

	//Checks to see that there are at least two control points: start and end poitns:
	if (!checkRobust())
		return;

	int size = controlPoints.size();
	float final = controlPoints[size - 1].time;
	Point start = controlPoints[0].position;
	Point end;

	//Draws all points but the last point
	for (float i = 0; i < final; i = i + window/2) {
		calculatePoint(end, i + window/2);
		DrawLib::drawLine(start, end, curveColor, curveThickness);
		start = end;
	}
	
	//Drawing the last point
	end = controlPoints[size-1].position;
	DrawLib::drawLine(start, end, curveColor, curveThickness);

	// Robustness: make sure there is at least two control point: start and end points
	// Move on the curve from t=0 to t=finalPoint, using window as step size, and linearly interpolate the curve points
	// Note that you must draw the whole curve at each frame, that means connecting line segments between each two points on the curve
	
	return;
#endif
}

// Sort controlPoints vector in ascending order: min-first
void Curve::sortControlPoints()
{
	std::sort(controlPoints.begin(), controlPoints.end(), [](const CurvePoint& lhs, const CurvePoint& rhs) { return lhs.time < rhs.time; });

	return;
}

// Calculate the position on curve corresponding to the given time, outputPoint is the resulting position
// Note that this function should return false if the end of the curve is reached, or no next point can be found
bool Curve::calculatePoint(Point& outputPoint, float time)
{
	// Robustness: make sure there is at least two control point: start and end points
	if (!checkRobust())
		return false;

	// Define temporary parameters for calculation
	unsigned int nextPoint;

	// Find the current interval in time, supposing that controlPoints is sorted (sorting is done whenever control points are added)
	// Note that nextPoint is an integer containing the index of the next control point
	if (!findTimeInterval(nextPoint, time))
		return false;

	// Calculate position at t = time on curve given the next control point (nextPoint)
	if (type == hermiteCurve)
	{
		outputPoint = useHermiteCurve(nextPoint, time);
	}
	else if (type == catmullCurve)
	{
		outputPoint = useCatmullCurve(nextPoint, time);
	}

	// Return
	return true;
}

// Check Roboustness
bool Curve::checkRobust()
{
	if (controlPoints.size() < 2) {
		return false;
	}
	return true;
}

// Find the current time interval (i.e. index of the next control point to follow according to current time)
bool Curve::findTimeInterval(unsigned int& nextPoint, float time)
{
	for (int i = 0; i < controlPoints.size() - 1; i++) {
		if (controlPoints[i].time <= time && controlPoints[i + 1].time > time) {
			nextPoint = i + 1;
			return true;
		}
	}
	return false;
}

// Implement Hermite curve
Point Curve::useHermiteCurve(const unsigned int nextPoint, const float time)
{
	Point newPosition;
	float normalTime, intervalTime;
	intervalTime = controlPoints[nextPoint].time - controlPoints[nextPoint - 1].time;
	normalTime = (time - controlPoints[nextPoint - 1].time) / intervalTime;
	float t = normalTime;
	Point p0 = controlPoints[nextPoint - 1].position;
	Point p1 = controlPoints[nextPoint].position;
	Vector m0 = controlPoints[nextPoint - 1].tangent;
	Vector m1 = controlPoints[nextPoint].tangent;
	newPosition = (2 * t*t*t - 3 * t*t + 1)*p0 + (-2 * t*t*t + 3 * t*t)*p1 +
		(t*t*t - 2 * t*t + t)*intervalTime*m0 + (t*t*t - t*t)*intervalTime*m1;

	// Return result
	return newPosition;

}

// Implement Catmull-Rom curve
Point Curve::useCatmullCurve(const unsigned int nextPoint, const float time)
{
	Point newPosition;

	float normalTime, intervalTime;
	intervalTime = controlPoints[nextPoint].time - controlPoints[nextPoint - 1].time;
	normalTime = (time - controlPoints[nextPoint - 1].time) / intervalTime;
	float t = normalTime;
	Point p0, p1, p2, p3;
	p1 = controlPoints[nextPoint - 1].position;
	p2 = controlPoints[nextPoint].position;
	if (nextPoint == 1) {
		p0 = controlPoints[nextPoint - 1].position - controlPoints[nextPoint - 1].tangent;
	}
	else {
		p0 = controlPoints[nextPoint - 2].position;
	}
	if(nextPoint == (controlPoints.size() - 1)) {
		p3 = controlPoints[nextPoint].position + controlPoints[nextPoint].tangent;
	}
	else {
		p3 = controlPoints[nextPoint + 1].position;
	}
	Point a, b, c, d;
	a = 0.5 * (2 * p1);
	b = 0.5 * (p2 + (p0*-1));
	c = 0.5 * (2 * p0 + (p1* -5) + 4 * p2 + (p3* -1));
	d = 0.5 * ((p0*-1) + 3 * p1 + (p2*-3) + p3);
	newPosition = a + (b * t) + (c * t * t) + (d * t * t * t);
	return newPosition;
}